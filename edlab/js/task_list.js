var m = require('mithril')
var Stream = require('mithril/stream')
var Markdown = require('./markdown').Markdown

var TaskList = function (vnode) {
    var tasks = []
    
    function loadTasks () {
        m.request({
            method: "GET",
            url: "/tasks.json"
        }).then(function (json) {
            tasks = json
            tasks.map(function (t) {t.open = Stream(false)})
        })
    }

    return {
        oninit: loadTasks,
        view: function () {
            return tasks.map(function (t) {
                return m('div', [
                    m(Markdown, {markdown: t.problem}),
                    m('button', {onclick: function () {t.open(!t.open())}}, t.open() ? 'Скрыть решение' : 'Показать решение'),
                    t.open() && m(Markdown, {markdown: t.solution}),
                    m('a', {href: '/task/edit?id=' + t.id}, 'Редактировать'),
                    m('hr')
                ])
            })
        }
    }
}

var content = document.getElementById('content')
m.mount(content, TaskList)
