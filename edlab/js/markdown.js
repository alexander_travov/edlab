var m = require('mithril')
var md = require('markdown-it')().use(require('./mdkatex'))
var Stackedit = require('stackedit-js')


var Markdown = {
    oncreate: function (vnode) {
        vnode.dom.innerHTML = md.render(vnode.attrs.markdown)
    },
    view: function (vnode) {
        return m('div')
    }
}


function MarkdownTextArea (vnode) {
    var markdown, preview

    function updatePreview () {
        preview.innerHTML = md.render(markdown())
    }

    function stackedit () {
        var st = new Stackedit()
        st.openFile({name: 'Задача', content: {text: vnode.attrs.markdown()}})
        st.on('fileChange', function (file) {markdown(file.content.text)})
        st.on('close', m.redraw)
    }

    return {
        oninit: function () {
            markdown = vnode.attrs.markdown
        },
        oncreate: function () {
            preview = vnode.dom.getElementsByClassName('preview')[0]
            updatePreview()
        },
        view: function () {
            return m('div', [
                m('.preview.bg-washed-green.pa1'),
                m('textarea.db.w-100', {
                    value: markdown(),
                    oninput: m.withAttr("value", function (md) {
                        markdown(md)
                        updatePreview()
                    })
                }),
                m('img[src=/static/stackedit-0.png][title=StackEdit]', {onclick: stackedit}),
            ])
        }
    }
}


module.exports = {
    Markdown: Markdown,
    MarkdownTextArea: MarkdownTextArea,
}
