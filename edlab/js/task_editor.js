var m = require('mithril')
var Stream = require('mithril/stream')
var MarkdownTextArea = require('./markdown').MarkdownTextArea
var Complexity = require('./complexity')
var guid = require('./guid')


var TaskEditor = function (vnode) {
    var message = null

    var categories = [] // Все категории

    var categoryIndex = Stream(null)

    var task = {
        problem: Stream(""),
        solution: Stream(""),
        answers: Stream([]),
        categories: Stream([]),
        complexity: Stream(0),
    }

    function loadTask(id) {
        message = "Данные задачи загружаются..."

        return m.request({
            method: "GET",
            url: "/task.json?" + m.buildQueryString({id: id}),
        }).then(function (json) {
            task.id = json.id
            task.problem(json.problem || "")
            task.solution(json.solution || "")
            if (json.answers && json.answers.length) {
                for (var i = 0; i < json.answers.length; i += 1) {
                    var a = json.answers[i]
                    a.text = Stream(a.text)
                    a.correct = Stream(a.correct)
                    a.key = guid()
                }
            }
            task.answers(json.answers || [])
            task.categories(json.categories || [])
            task.complexity(json.complexity || 0)
            message = null
        }).catch(function (e) {
            message = "Не удалось получить данные о задаче " + id
        })
    }

    function saveTask () {
        var answers = []
        for (var i = 0; i < task.answers().length; i += 1) {
            var ta = task.answers()[i]
            answers.push({text: ta.text(), correct: ta.correct()})
        }

        var data = {
            problem: task.problem(),
            solution: task.solution(),
            answers: answers,
            complexity: task.complexity(),
            categories: task.categories(),
        }

        if (task.id) data.id = task.id

        return m.request({
            method: "PUT",
            url: "/task/edit",
            data: data
        }).then(function (json) {
            task.id = json.id
            if (!args.id) window.location.search = m.buildQueryString({id: task.id})
        })
    }

    function loadCategories () {
        return m.request({
            method: "GET",
            url: "/categories.json",
        }).then(function (json) {
            categories = json
        })
    }

    function addAnswer () {
        task.answers().push({
            correct: Stream(false),
            text: Stream(""),
            key: guid()
        })
    }

    function removeAnswer (answer) {
        var answers = task.answers()
        var i = answers.indexOf(answer)
        if (i >= 0) answers.splice(i, 1)
    }

    function addCategory () {
        if (categoryIndex() !== null) {
            task.categories().push(categories[categoryIndex()])
            categoryIndex(null)
        }
    }

    function removeCategory (category) {
        var categories = task.categories()
        var i = categories.indexOf(category)
        if (i >= 0) categories.splice(i, 1)
    }

    return {
        oninit: function () {
            loadCategories()
            vnode.attrs.id && loadTask(vnode.attrs.id)
        },
        view: function () {
            if (message) return message
            return m('.pa3', [
                m('h1', 'Условие:'),
                m(MarkdownTextArea, {markdown: task.problem}),

                m('div', [
                    'Категории:',
                    task.categories().map(function (category) {
                        return m('span.bg-light-green.ma1', {key: category}, [
                            category,
                            m('i.fa.fa-close', {
                                onclick: function (c) {
                                    return function () {removeCategory(c)}
                                }(category)
                            })
                        ])
                    }),
                    m('select.db', {selectedIndex: categoryIndex(), onchange: m.withAttr('selectedIndex', categoryIndex)}, categories.map(function (c) {
                        return m('option', {disabled: task.categories().indexOf(c) > -1}, c)
                    })),
                    m('button', {onclick: addCategory}, 'Добавить категорию'),
                ]),

                m('div', ['Сложность:', m(Complexity, {complexity: task.complexity})]),

                m('h2', 'Варианты ответа:'),
                m('button', {onclick: addAnswer}, 'Добавить вариант'),
                m('table.f6.w-100', task.answers().map(function (answer) {
                    return m('tr', {key: answer.key}, [
                        m('td', m('input.center[type=checkbox]', {checked: answer.correct(), onchange: m.withAttr('checked', answer.correct)})),
                        m('td', m(MarkdownTextArea, {markdown: answer.text})),
                        m('i.fa.fa-close', {
                            onclick: function (a) {
                                return function () {removeAnswer(a)}
                            }(answer)
                        })
                    ])
                })),

                m('h1', 'Решение:'),
                m(MarkdownTextArea, {markdown: task.solution}),

                m('button', {onclick: saveTask}, 'Сохранить'),
            ])
        }
    }
}


var content = document.getElementById('content')
var args = m.parseQueryString(window.location.search)

m.mount(content, {
    view: function () {
        return m(TaskEditor, {id: args.id})
    }
})
