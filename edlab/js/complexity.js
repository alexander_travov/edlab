var m = require('mithril')
var Stream = require('mithril/stream')

module.exports = function (vnode) {
    var max = 5
    var stars = []
    for (var i = 0; i < max; i += 1) stars.push(i+1)
    var complexity
    var current = Stream(0)

    return {
        oninit: function () {
            complexity = vnode.attrs.complexity
            current(complexity())
        },
        view: function () {
            return stars.map(function (s) {
                return m('i', {
                    className: 'fa ' + (s <= current() ? 'fa-star' : 'fa-star-o'),
                    onmouseover: function () {current(s)},
                    onmouseout: function () {current(complexity())},
                    onclick: function () {current(s); complexity(s)}
                })
            })
        }
    }
}
