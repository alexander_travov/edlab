var m = require('mithril')
var Stream = require('mithril/stream')
var Markdown = require('./markdown').Markdown

var Training = function (vnode) {
    var done = Stream(false)

    var tasks = []
    
    function loadTasks () {
        m.request({
            method: "GET",
            url: "/tasks.json"
        }).then(function (json) {
            tasks = json
            tasks.map(function (t) {
                t.answers.map(function (a) {
                    a.checked = Stream(false)
                })
            })
        })
    }

    return {
        oninit: loadTasks,
        view: function () {
            return [
                tasks.map(function (t) {
                    return m('div', [
                        m(Markdown, {markdown: t.problem}),
                        done() && m(Markdown, {markdown: t.solution}),
                        m('table.f6.w-100', t.answers.map(function (answer) {
                            return m('tr', {
                                    className: done() && (
                                        answer.checked() && answer.checked() !== answer.correct && 'bg-light-red' ||
                                        answer.correct && 'bg-light-green'
                                    )
                                }, [
                                m('td', m('input.center[type=checkbox]', {
                                    checked: answer.checked(), onchange: m.withAttr('checked', answer.checked)})),
                                m('td', m(Markdown, {markdown: answer.text})),
                            ])
                        })),
                        m('hr')
                    ])
                }),
                m('button', {onclick: function () {done(true)}}, 'Проверка')
            ]
        }
    }
}

var content = document.getElementById('content')
m.mount(content, Training)
