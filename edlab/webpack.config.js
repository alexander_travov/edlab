"use strict"

module.exports = {
    entry: {
        'task_editor': __dirname + "/js/task_editor.js",
        'task_list': __dirname + "/js/task_list.js",
        'training': __dirname + "/js/training.js",
    },
    output: {
        path: __dirname + "/static",
        filename: "[name].js"
    }
}
