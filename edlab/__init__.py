# -*- coding: utf8 -*-

import os
import json
import click
from flask import Flask, render_template, jsonify, request, abort
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Table, String, Integer, DateTime, ForeignKey
from sqlalchemy.types import TypeDecorator

app = Flask(__name__)
DB_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'app.db')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DB_PATH
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class StringyJSON(TypeDecorator):
    impl = String

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value


task_category_table = Table(
    'task_category',
    db.Model.metadata,
    Column('task_id', Integer, ForeignKey('task.id')),
    Column('category_id', Integer, ForeignKey('category.id'))
)


class Task(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String)
    problem = Column(String)
    solution = Column(String)
    complexity = Column(Integer)
    modification_dt = Column(DateTime)
    answers = Column(StringyJSON)
    categories = db.relationship(
        'Category',
        secondary=task_category_table,
        back_populates='tasks'
    )

    def __repr__(self):
        return '<Task(name=%s)>' % self.name

    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'problem': self.problem,
            'solution': self.solution,
            'answers': self.answers,
            'complexity': self.complexity,
            'categories': [c.name for c in self.categories]
        }



class Category(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String)
    tasks = db.relationship(
        'Task',
        secondary=task_category_table,
        back_populates='categories'
    )

    def __repr__(self):
        return '<Category(name=%s)>' % self.name


@click.command('init-db')
def init_db():
    db.create_all()

    for cat in (
            'Неравенство',
            'Логарифм',
            'Показательная функция',
            'Корень',
            'Тригонометрия',
            'Модуль',
            'Задача с параметром',
            'Окружность',
            'Планиметрия',
            'Стереометрия',
            'Арифметическая прогрессия',
            'Геометрическая прогрессия',
            'Квадратное уравнение',
            'Замена переменной',
    ):
        db.session.add(Category(name=cat))
    db.session.commit()

    click.echo('Initialised db')

app.cli.add_command(init_db)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/categories.json')
def categories_json():
    categories = Category.query.all()
    return jsonify([c.name for c in categories])


@app.route('/task.json')
def task_json():
    task_id = request.args.get('id', None)
    if task_id is None:
        abort(404)
    try:
        task_id = int(task_id)
    except ValueError:
        abort(404)
    task = Task.query.filter_by(id=task_id).first()
    if task is None:
        abort(404)
    return jsonify(task.as_dict())


@app.route('/task/edit', methods=['GET', 'PUT'])
def task_edit():
    if request.method == 'GET':
        return render_template('task.html')
    elif request.method == 'PUT':
        task = json.loads(request.data)
        if task.get('id'):
            db_task = Task.query.filter_by(id=task['id']).first()
        else:
            db_task = Task()
        print(db_task)
        for key in task:
            print(key, task[key], type(task[key]))
            if key == 'id':
                continue
            elif key == 'categories':
                categories = set(task['categories'])
                db_categories = set(c.name for c in db_task.categories)
                for c in db_categories.difference(categories):
                    db_task.categories.remove(Category.query.filter_by(name=c).first())
                for c in categories.difference(db_categories):
                    db_task.categories.append(Category.query.filter_by(name=c).first())
            else:
                setattr(db_task, key, task[key])
        print('Adding to session')
        db.session.add(db_task)
        print('Commiting session')
        db.session.commit()
        print('Commited session')
        return jsonify({'id': db_task.id})


@app.route('/task_list')
def task_list():
    return render_template('tasks.html')


@app.route('/tasks.json')
def tasks_json():
    tasks = Task.query.all()
    return jsonify([t.as_dict() for t in tasks])


@app.route('/training')
def training():
    return render_template('training.html')
